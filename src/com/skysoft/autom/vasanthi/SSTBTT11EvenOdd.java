package com.skysoft.autom.vasanthi;

import java.util.Scanner;

public class SSTBTT11EvenOdd {
	public static void main(String[] args) 
	{
		int number;
		System.out.println("Enter number to check whether it is even or odd.");
		Scanner sc = new Scanner(System.in);
		number = sc.nextInt();

		if(number % 2 == 0)
		{
			System.out.print(number);
			System.out.println(" is a even number");
		}
		else
		{
			System.out.print(number);
			System.out.println(" is a odd number");
		}
	}
}
