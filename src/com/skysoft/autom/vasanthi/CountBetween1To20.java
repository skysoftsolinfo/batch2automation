package com.skysoft.autom.vasanthi;

public class CountBetween1To20 {

	public static void main(String[] args) 
	{
		System.out.println("Count between 1 to 20");
		int start =1;
		int end =20;
		countNumber(start,end);
	}
	public static void countNumber(int start,int end)
	{
		int count = 1;
		for(int number = start;number < end;number ++)
		{
			if(number % 5 == 0)
			{
				count ++;
			}
		}
		System.out.println(count);
	}
}
