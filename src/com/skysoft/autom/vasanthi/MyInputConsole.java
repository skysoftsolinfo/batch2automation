package com.skysoft.autom.vasanthi;
import java.io.IOException;
import java.util.Scanner;

public class MyInputConsole {
	public static void main(String[] args) throws IOException {		
		String name = System.console().readLine();
		System.out.println(name);
	}
}
