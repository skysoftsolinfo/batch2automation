package com.skysoft.autom.vasanthi;

public class SSTBTT11DivisibleBy3
{
	public static void main(String[] args) 
	{
		System.out.println("Numbers divisible by 3 between 1 to 100");
		
		for(int number = 1; number < 100; number ++)
		{
			if(number % 3 == 0)
			{
				System.out.println(number);
			}
		}
	}
}

