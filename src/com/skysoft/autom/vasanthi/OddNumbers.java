package com.skysoft.autom.vasanthi;
public class OddNumbers 
{
	public static void main(String[] args)
	{
		System.out.println("Odd numbers between 1 to 20");
		int start = 1;
		int end = 20;
		oddNumbers(start, end);
	}
	public static void oddNumbers(int start, int end)
	{
		for(int number = start;number < end;number ++)
		{
			if(number % 2 !=0)
			{
				System.out.println(number);
			}
			
		}
		
	}

}
