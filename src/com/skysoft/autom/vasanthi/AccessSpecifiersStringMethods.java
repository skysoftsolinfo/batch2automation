package com.skysoft.autom.vasanthi;
public class AccessSpecifiersStringMethods 
{
	  AccessSpecifiersLoopConsolidations access;

 public static void main(String[] args) 
 {
  String str1 = "Hello Welcome Hello Welcome Again";
  stringCount1(str1);

  String str2 = "Hello Welcome Hello Welcome Again";
  PassingString(str2);

  String str3 = "hello welcome hello welcome vasu";
  stringIdentify1(str3);


  String str4 = "hello welcome hello welcome vasu";
  stringIdentify4(str4);
  //int exce=Integer.parseInt(str4);
 }
 public static void stringCount1(String sa)
 {
  String words[] = sa.split(" ");

  for (int i = 0; i < words.length; i++)
  {
   int count = 0;
   for (int j = 0; j < words.length; j++) 
   {
    if (words[i].equals(words[j]))
    {
     count++;
    }
   }
   System.out.println("The count of " + words[i] + " = " + count);
  }
  	System.out.println('\n');
}

 private static void PassingString(String sb)
 {
  String words[] = sb.split(" ");
  System.out.println("Number of words in a String=" + words.length);
  System.out.println('\n');

 }

 protected static void stringIdentify1(String sc) 
 {
  System.out.println(sc.substring(0, 19));
  System.out.println('\n');
 }

 static void stringIdentify4(String sd)
 {
  System.out.println(sd.substring(19));
  System.out.println('\n');
 }

}