package com.skysoft.autom.vasanthi;
public class FindingWordsOnString 
{
	public static void main(String[] args) 
	{
		String str = "Hello Welcome Hellow Welcome Again";
		PassingString(str);
	}
	public static void PassingString(String s)
	{
		String words[ ] = s.split(" ");
		System.out.println("Number of words in a String=" + words.length);
	}
}
