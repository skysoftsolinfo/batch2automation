package com.skysoft.autom.vasanthi;
public class NumberDivisibleBy3 {

	public static void main(String[] args) 
	{
		System.out.println("Numbers divisible by 3 between 1 to 20");
		int start = 1;
		int end = 20;
		divisible(start, end);
	}
	public static void divisible(int start, int end)
	{
		for(int number = start;number < end;number ++)
		{
			if(number % 3==0)
			{
				System.out.println(number);
			}
		}
	}
}

