/*
 * This is a SingleLevel Char and Loops operation implementations   
 * This class extended from MergeCharAndLoops operations.
 */
package com.skysoft.autom.vasanthi;
public class ChildMergeCharAndLoops extends MergeCharAndLoops
{
	public static void main(String[] args) 
	{
		// SingleLevel Char operations
		MergeCharAndLoops obj=new MergeCharAndLoops();
		obj.evenPosition("gotobed");
		obj.oddPosition("gotobed");
		
		// SingleLevel Loop operations
		System.out.println("Count between 1 to 20 which is divisible by 5");
		obj.countNumber(1, 20);
		System.out.println("Even numbers between 1 to 20");
		obj.evenNumbers(1, 20);
		System.out.println("Odd numbers between 1 to 20");
		obj.oddNumbers(1,20);
		System.out.println("Numbers divisible by 3 between 1 to 20");
		obj.divisible(1, 20);
		System.out.println("Enter 1 for Addition, 2 for Substraction, 3 for Multiplication, 4 for Division, 5 for exit");
		obj.operationComputation(1, 10, 2);
		obj.operationComputation(2, 10, 2);
		obj.operationComputation(3, 10, 2);
		obj.operationComputation(4, 10, 2);
		obj.operationComputation(5, 10, 2);


	}

}
