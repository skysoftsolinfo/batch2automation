package com.skysoft.autom.vasanthi;

public class ChildAccessLoops {

	public static void main(String[] args) 
	{
		AccessSpecifiersLoopConsolidations loop=new AccessSpecifiersLoopConsolidations();
		System.out.println("Count between 1 to 20 which is divisible by 5");
		loop.countNumber(1, 20);
		System.out.println("Even numbers between 1 to 20");
		loop.evenNumbers(1, 20);
		//System.out.println("Odd numbers between 1 to 20");
		//loop.oddNumbers(1,20);
		System.out.println("Numbers divisible by 3 between 1 to 20");
		loop.divisible(1, 20);
		
	}
}
