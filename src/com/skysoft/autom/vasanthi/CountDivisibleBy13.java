package com.skysoft.autom.vasanthi;
public class CountDivisibleBy13 
{
	public static void main(String[] args) 
	{
		int a[ ] = { 13,26,2,5,6,39 };
		divisible(a);
	}
	public static void divisible(int a[])
	{
		int count = 1;
		for(int i = 0 ; i < a[i] ; i ++)
		{
			if(a[i] % 13 == 0)
			{
				count ++;
			}
		}
		System.out.println("Count divisible by 13 = " +count);
}
}
