/*
 * This is a multilevel Char,Arrays,Strings operation implementations        
 */
package com.skysoft.autom.vasanthi;

public class ChildMultiLevel {

	public static void main(String[] args) 
	{
		// multilevel char operations
		MultiLevelCharOperations obj = new MultiLevelCharOperations();
		obj.evenPosition("gotobed");
		obj.oddPosition("gotobed");

		// multilevel Array operations
		int a[ ] = { 2,6,1,4,9,5 };
		obj.firstHighest(a);
		int b[] = { 2,6,1,4,9,5 };
		obj.secondLowest(b);	
		int c[ ] = { 7,14,21,4,3,6,9 };
		obj.divisible(c);
	    int[] numbers = { 1,2,3,3,2,1};
	    obj.duplicate(numbers);
	    int d[]={1,2,1,3,3};
	    obj.uniquemethod(d);
	    
		// multilevel String operations
	    obj.stringCount1("Hello Welcome Hello Welcome Again");
	    obj.PassingString("Hello Welcome Hello Welcome Again");
	    obj.stringIdentify1("hello welcome hello welcome vasu");
	    obj.stringIdentify4("hello welcome hello welcome vasu");
	    obj.stringCount2("hello welcome hello welcome");
	    obj.stringCount("hello welcome hello welcome");
	    obj.stringIdentify("hello welcome hello welcome");
	    
	    
	}

}
