/*
 * This is a Singlelevel String and Arrays operation implementations        
 */
package com.skysoft.autom.vasanthi;

import java.util.ArrayList;
import java.util.Arrays;

public class MergeStringsAndArrays {

	public static void main(String[] args) 
	{
		  String str1 = "Hello Welcome Hello Welcome Again";
		  stringCount1(str1);

		  String str2 = "Hello Welcome Hello Welcome Again";
		  PassingString(str2);

		  String str3 = "hello welcome hello welcome vasu";
		  stringIdentify1(str3);


		  String str4 = "hello welcome hello welcome vasu";
		  stringIdentify4(str4);
		  
		  try
		  {
		  int exc=Integer.parseInt(str4);  //  java.lang.NumberFormatException
		 }
		  catch(Exception e)
		  {
			  System.out.println(e);
		  }
		  


		  String str5 = "hello welcome hello welcome";
		  stringCount2(str5);

		  String str6 = "hello welcome hello welcome";
		  stringCount(str6);


		  String str7 = "hello welcome, hello welcome";
		  stringIdentify(str7);
		  
		  int a[ ] = { 2,6,1,4,9,5 };
		  firstHighest(a);
		  try
			{
				int b[] = {};    //java.lang.ArrayIndexOutOfBoundsException
				secondLowest(b);
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			
		  int b[] = { 2,6,1,4,9,5 };
		  secondLowest(b);
			
		  int c[ ] = { 7,14,21,4,3,6,9 };
		  divisible(c);
			
		  int[] numbers = { 1,2,3,3,2,1};
		  duplicate(numbers);
			 
		  int d[]={1,2,1,3,3};
		  uniquemethod(d);
	      ArrayList<Integer> unique = new ArrayList<Integer>();
		  
	 }
	 public static void stringCount1(String sa)
	 {
		/*
		 * Method to compute and identify string count at given string.
		 */
	  String words[] = sa.split(" ");

	  for (int i = 0; i < words.length; i++)
	  {
	   int count = 0;
	   for (int j = 0; j < words.length; j++) 
	   {
	    if (words[i].equals(words[j]))
	    {
	     count++;
	    }
	   }
	   System.out.println("The count of " + words[i] + " = " + count);
	  }
	  	System.out.println('\n');
	}

	 public static void PassingString(String sb)
	 {
		/*
		* Method to compute and identify number of words at given string.
		*/
	  String words[] = sb.split(" ");
	  System.out.println("Number of words in a String=" + words.length);
	  System.out.println('\n');

	 }

	 public static void stringIdentify1(String sc) 
	 {
		/*
		* Method to compute and identify first 3 words at given string.
		*/
	  System.out.println(sc.substring(0, 19));
	  System.out.println('\n');
	 }

	 public static void stringIdentify4(String sd)
	 {
		/*
		* Method to compute and identify Last 2 words at given string.
		*/
	  System.out.println(sd.substring(19));
	  System.out.println('\n');
	 }


	 public static void stringCount2(String se) 
	 {
	   /*
		* Method to compute and identify Replace welcome with world at given string.
		*/
	  String replaceString = se.replace("welcome", " world"); //replaces all occurrences of welcome to world  
	  System.out.println(replaceString);
	  System.out.println('\n');
	 }

	 public static void stringCount(String sf) 
	 {
	   /*
		* Method to compute and identify Replace hello with hey at given string.
		*/
	  String replaceString = sf.replace("hello", "Hey"); //replaces all occurrences of hello to Hey  
	  System.out.println(replaceString);
	  System.out.println('\n');
	 }

	 public static void stringIdentify(String sg) 
	 {
	   /*
		* Method to compute and identify Replace hello with welcome at given string.
		*/
	  String replaceString = sg.replace("hello", "welcome"); //replaces all occurrences of hello to a welcome
	  System.out.println(replaceString);
	 }
		 
	 public static void firstHighest(int a[])
		{
			/*
			 * Method to compute and identify First Highest number in a given array.
			 */
			int number = a.length;
			Arrays.sort(a);
			System.out.println(Arrays.toString(a));
			int result = a[number - 1];
			System.out.println("Largest number is =" + result);
			System.out.println('\n');
		}
		public static void secondLowest(int b[])
		{
			/*
			 * Method to compute and identify Second Lowest number in a given array.
			 */
			int number = b.length;
			Arrays.sort(b);
			System.out.println(Arrays.toString(b));
			int result = b[1];
			System.out.println("Largest number is =" + result);
		    System.out.println('\n');

		}
		public static void divisible(int c[])
		{
			/*
			 * Method to compute and identify count of the array which is divisible by 7
			 */
			int count = 0;
			for(int i = 0 ; i < c[i] ; i ++)
			{
				if(c[i] % 7 == 0)
				{
					count ++;
				}
			}
			System.out.println("Count divisible by 7 = " +count);
		    System.out.println('\n');

		}
		
		public static void duplicate(int numbers[])
		{
			/*
			 * Method to compute and identify Duplicates numbers of the given array 
			 */
				Arrays.sort(numbers);
			    int previous = numbers[0] - 1;
			    int dupCount = 0;
			    for (int i = 0; i < numbers.length; ++i)
			    {
			        if (numbers[i] == previous) 
			        {
			            ++dupCount;
			        } else
			        {
			            previous = numbers[i];
			        }
			    }
			    System.out.println("There were " + dupCount + " duplicate elements in the array.");
			    System.out.println('\n');

		}
		public static void uniquemethod(int d[])
		{
			/*
			 * Method to compute and identify Unique numbers of the given array 
			 */
				ArrayList<Integer> unique = new ArrayList<Integer>();
				for(int i = 0 ; i <d.length-1 ; i ++)
				{
					int count = 0;
					for(int j = 0; j <d.length; j++)
					{
						if((d[i] == d[j]))
							count++;
					}
					if (count == 1)
						unique.add(d[i]);
				}
				System.out.println(unique.size());
				System.out.println(unique);
		}

		}
	

