/*
 * This is a SingleLevel Arrays and Strings operation implementations   
 * This class extended from MergeStringsAndArrays operations.
 */
package com.skysoft.autom.vasanthi;

import java.util.ArrayList;

public class ChildMergeStringAndArrays extends MergeStringsAndArrays{

	public static void main(String[] args) 
	{
		// SingleLevel String operations
		ChildMergeStringAndArrays obj=new ChildMergeStringAndArrays();
		obj.stringCount1("Hello Welcome Hello Welcome Again");
		obj.PassingString("Hello Welcome Hello Welcome Again");
		obj.stringIdentify1("hello welcome hello welcome vasu");
		obj.stringIdentify4("hello welcome hello welcome vasu");
		obj.stringCount2("hello welcome hello welcome");
		obj.stringIdentify("hello welcome hello welcome");
		obj.stringCount("hello welcome hello welcome");
		
		// SingleLevel Arrays operations
		System.out.println("Arrays");
		int a[ ] = { 2,6,1,4,9,5 };
		obj.firstHighest(a);
		int b[] = { 2,6,1,4,9,5 };
		secondLowest(b);
		int c[ ] = { 7,14,21,4,3,6,9 };
		divisible(c);
		int[] numbers = { 1,2,3,3,2,1};
		duplicate(numbers);
	    int d[]={1,2,1,3,3};
		uniquemethod(d);
	   }

}
