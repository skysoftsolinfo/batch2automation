package com.skysoft.autom.vasanthi;
public class EvenNumbers 
{
	public static void main(String[] args)
	{
		System.out.println("Even numbers between 1 to 20");
		int start = 1;
		int end = 20;
		evenNumbers(start, end);
		
	}
	public static void evenNumbers(int start, int end)
	{
		for(int number=start; number<end; number++)
		{
			if(number%2==0)
			{
				System.out.println(number);
			}
		}
	}
}