package com.skysoft.autom.vasanthi;

public class CountOfRepeatingWords 
{
	public static void main(String[] args) 
	{
		String str = "Hello Welcome Hellow Welcome Again";
		stringCount(str);
	}
		public static void stringCount(String s)
		{
		String words[] = s.split(" ");
		
		for(int i = 0 ; i < words.length ; i ++)
		{
			int count = 0;
			for(int j = 0 ; j < words.length ; j ++)
			{
				if(words[i].equals(words[j]))
				{
					count ++;
				}
			}
			System.out.println("The count of " + words[i] + " = " + count);
		}
	}
}
