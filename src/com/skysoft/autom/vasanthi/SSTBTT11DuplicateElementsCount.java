package com.skysoft.autom.vasanthi;
import java.util.Arrays;
public class SSTBTT11DuplicateElementsCount 
{
	public static void main(String[] args)
	{
		  int[] numbers = { 1,2,3,3,2,1};
		    Arrays.sort(numbers);
		    int previous = numbers[0] - 1;
		   
		    int dupCount = 0;
		
		    for (int i = 0; i < numbers.length; ++i)
		    {
		        if (numbers[i] == previous) 
		        {
		            ++dupCount;
		        } else
		        {
		            previous = numbers[i];
		        }
		    }
		    System.out.println("There were " + dupCount + " duplicate elements in the array.");
	}

}
