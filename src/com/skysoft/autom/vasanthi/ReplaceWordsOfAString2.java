package com.skysoft.autom.vasanthi;
public class ReplaceWordsOfAString2 
{
	public static void main(String[] args) 
	{
		String str = "hello welcome hello welcome"; 
		stringCount(str);
	}
	public static void stringCount(String s)
	{
		String replaceString = s.replace("hello","Hey");//replaces all occurrences of hello to Hey  
		System.out.println(replaceString);
	}

}
