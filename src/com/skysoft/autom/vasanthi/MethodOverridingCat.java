package com.skysoft.autom.vasanthi;

public class MethodOverridingCat extends MethodOverridingAnimal
{
	public void eat()   //eat() method overriden by MethodOverridingCat class.
    {
        System.out.println("Dog eats meat");
        System.out.println("It shouts bow-bow");

    }
}
