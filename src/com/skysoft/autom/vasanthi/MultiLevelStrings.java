/*
 * This is a multilevel String operation implementations        
 */
package com.skysoft.autom.vasanthi;
public class MultiLevelStrings {

	public static void main(String[] args) 
	{
		
		  String str1 = "Hello Welcome Hello Welcome Again";
		  stringCount1(str1);

		  String str2 = "Hello Welcome Hello Welcome Again";
		  PassingString(str2);
		
		  String str3 = "hello welcome hello welcome vasu";
		  stringIdentify1(str3);

		  String str4 = "hello welcome hello welcome vasu";
		  stringIdentify4(str4);
		  try
		  {
		  int exc=Integer.parseInt(str4);  //  java.lang.NumberFormatException
		 }
		  catch(Exception e)
		  {
			  System.out.println(e);
		  }
		  

		  String str5 = "hello welcome hello welcome";
		  stringCount2(str5);

	      String str6 = "hello welcome hello welcome";
		  stringCount(str6);

		  String str7 = "hello welcome, hello welcome";
		  stringIdentify(str7);

		 }
		 public static void stringCount1(String sa)
		 {
			/*
			 * Method to compute and identify string count at given string.
			 */
		  String words[] = sa.split(" ");

		  for (int i = 0; i < words.length; i++)
		  {
		   int count = 0;
		   for (int j = 0; j < words.length; j++) 
		   {
		    if (words[i].equals(words[j]))
		    {
		     count++;
		    }
		   }
		   System.out.println("The count of " + words[i] + " = " + count);
		  }
		  	System.out.println('\n');
		}

		 public static void PassingString(String sb)
		 {
			/*
			* Method to compute and identify number of words at given string.
			*/
		  String words[] = sb.split(" ");
		  System.out.println("Number of words in a String=" + words.length);
		  System.out.println('\n');

		 }

		 public static void stringIdentify1(String sc) 
		 {
			/*
			* Method to compute and identify first 3 words at given string.
			*/
		  System.out.println(sc.substring(0, 19));
		  System.out.println('\n');
		 }

		 public static void stringIdentify4(String sd)
		 {
			/*
			* Method to compute and identify Last 2 words at given string.
			*/
		  System.out.println(sd.substring(19));
		  System.out.println('\n');
		 }


		 public static void stringCount2(String se) 
		 {
		   /*
			* Method to compute and identify Replace welcome with world at given string.
			*/
		  String replaceString = se.replace("welcome", " world"); //replaces all occurrences of welcome to world  
		  System.out.println(replaceString);
		  System.out.println('\n');
		 }

		 public static void stringCount(String sf) 
		 {
		   /*
			* Method to compute and identify Replace hello with hey at given string.
			*/
		  String replaceString = sf.replace("hello", "Hey"); //replaces all occurrences of hello to Hey  
		  System.out.println(replaceString);
		  System.out.println('\n');
		 }

		 public static void stringIdentify(String sg) 
		 {
		   /*
			* Method to compute and identify Replace hello with welcome at given string.
			*/
		  String replaceString = sg.replace("hello", "welcome"); //replaces all occurrences of hello to a welcome
		  System.out.println(replaceString);
		 }
}

	