/*
 * This is a Singlelevel Char and Loops operation implementations        
 */
package com.skysoft.autom.vasanthi;
import java.util.Scanner;

public class MergeCharAndLoops {

	public static void main(String[] args) 
	{
		String str = "gotobed";
		evenPosition(str);
		String str1 = "gotobed";
		oddPosition(str);
		
		System.out.println("Count between 1 to 20");
		int start =1;
		int end =20;
		countNumber(start,end);
		
		System.out.println("Even numbers between 1 to 20");
		int startIndex = 1;
		int endIndex = 20;
		evenNumbers(startIndex,endIndex);
		
		System.out.println("Odd numbers between 1 to 20");
		int start1 = 1;
		int end1 = 20;
		oddNumbers(start1, end1);
		
		System.out.println("Numbers divisible by 3 between 1 to 20");
		int start2 = 1;
		int end2 = 20;
		divisible(start2, end2);
		
		int number;
		int firstNum;
		int secondNum;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first number");
		firstNum = sc.nextInt();
		System.out.println("Enter Second number");
		secondNum = sc.nextInt();
		int operation = 0;
		while(operation != 5)
		{
			System.out.println("Enter 1 for Addition, 2 for Substraction, 3 for Multiplication, 4 for Division, 5 for exit");
			operation = sc.nextInt();
			operationComputation(operation, firstNum, secondNum);
	    }
	
	}
	public static void evenPosition(String s)
	{
		/*
		 * Method to compute and identify string chars at even positions.
		 */
		int even = 0;
		char str[] = new char[4];
		for(int i = 0 ; i < s.length() ; i ++)
		{
		if(i % 2 == 0)
		{
			str[even] = s.charAt(i);
			even ++;
		}
		try
		{
		int exc=even/0;    //java.lang.ArithmeticException
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
		System.out.println("Even position in a given string is ::");
		System.out.println(str);
		System.out.println('\n');
	}

	public static void oddPosition(String s)
	{
		/*
		 * Method to compute and identify string chars at odd positions.
		 */
		int odd = 0;
		char strodd[] = new char[3];
		for(int i = 0 ; i <s.length() ; i ++)
		{
			if(i % 2 == 1)
			{
				strodd[odd] = s.charAt(i);
				odd++;
			}
		}
		System.out.println("Odd position in a given string is ::");
		System.out.println(strodd);
	}

	public static void countNumber(int start,int end)
	{
		/*
		 * Method to compute and identify Count which is divisible by 5 between 1 to 20.
		 */
		int count = 1;
		for(int number = start;number < end;number ++)
		{
			if(number % 5 == 0)
			{
				count ++;
			}
			
			try
			{
			int exc=number/0; 
			//java.lang.ArithmeticException
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		System.out.println(count);
	}
	public static void evenNumbers(int startIndex, int endIndex)
	{
		/*
		 * Method to compute and identify Even numbers between 1 to 20.
		 */
		for(int number=startIndex; number<endIndex; number++)
		{
			if(number%2==0)
			{
				System.out.println(number);
			}
		}
	}
	public static void oddNumbers(int start1, int end1)
	{
		/*
		 * Method to compute and identify Odd numbers between 1 to 20.
		 */
		for(int number = start1;number < end1;number ++)
		{
			if(number % 2 !=0)
			{
				System.out.println(number);
			}	
		}
	}
	public static void divisible(int start2, int end2)
	{
		/*
		 * Method to compute and identify Numbers which is divisible by 3 between 1 to 20.
		 */
		for(int number = start2;number < end2;number ++)
		{
			if(number % 3==0)
			{
				System.out.println(number);
			}
		}
	}
	public static void operationComputation(int operation, int firstNum, int secondNum)
	{
		/*
		 * Method to compute and identify Different Arithmetic operations like(AdditionSubtraction,Multiplication,Division)
		 */
		if(operation == 1)
		{
			System.out.println("The Sum of two numbers is ");
			System.out.println(firstNum + secondNum);
		}
		else if(operation == 2)
		{
			System.out.println("The Sub of two numbers is ");
			System.out.println(firstNum - secondNum);		}
		
		else if(operation == 3)
		{
			System.out.println("The Mul of two numbers is ");
			System.out.println(firstNum * secondNum);	
		}
		else if(operation == 4)
		{	
			System.out.println("The Div of two numbers is ");
			System.out.println(firstNum / secondNum);	
		}

	}
	
	

}
