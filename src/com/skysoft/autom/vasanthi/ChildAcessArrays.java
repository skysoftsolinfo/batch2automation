package com.skysoft.autom.vasanthi;

public class ChildAcessArrays {

	public static void main(String[] args)
	{
		AccessSpecifiersArraysComputations arr=new AccessSpecifiersArraysComputations();
		int a[ ] = { 2,6,1,4,9,5 };
		arr.firstHighest(a);
		
		int b[] = { 2,6,1,4,9,5 };
		arr.secondLowest(b);
		
		// int c[ ] = { 7,14,21,4,3,6,9 };
		// arr.divisible(c);
		 
		 int[] numbers = { 1,2,3,3,2,1};
		 arr.duplicate(numbers);

		}

}
