package com.skysoft.autom.java.web;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginTestCase {
	/**
     * @param args
     * @throws InterruptedException
     * @throws IOException 
     */
    public static void main(String[] args) throws InterruptedException, IOException {
        // Telling the system where to find the Chrome driver
        System.setProperty(
                "webdriver.chrome.driver",
                "C:\\Users\\Seats\\Downloads\\chromedriver_win32\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();

        // Open sentrifugo application
        webDriver.get("http://192.168.1.8/sentrifugo");

        WebElement username = webDriver.findElement(By.id("username"));
        username.sendKeys("skysf0001");
        
        WebElement passcode = webDriver.findElement(By.id("password"));
        passcode.sendKeys("skysoft1");

        WebElement submit = webDriver.findElement(By.id("loginsubmit"));
        submit.click();

        // Printing result here.
        System.out.println(passcode);

        webDriver.close();
        webDriver.quit();
    }

}
