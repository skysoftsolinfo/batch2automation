package com.skysoft.autom.pavani;

import java.util.Scanner;

public class Loopconsolidations 
{
	public static void main(String[] args) 
	{
		System.out.println("Count between 1 to 20");
		int start =1;
		int end =20;
		countNumber(start,end);
		
		System.out.println("Even numbers between 1 to 20");
		int startIndex = 1;
		int endIndex = 20;
		evenNumbers(startIndex,endIndex);
		
		System.out.println("Odd numbers between 1 to 20");
		int start1 = 1;
		int end1 = 20;
		oddNumbers(start1, end1);
		
		System.out.println("Numbers divisible by 3 between 1 to 20");
		int start2 = 1;
		int end2 = 20;
		divisible(start2, end2);
		
		int number;
		int firstNum;
		int secondNum;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first number");
		firstNum = sc.nextInt();
		System.out.println("Enter Second number");
		secondNum = sc.nextInt();
		int operation = 0;
		while(operation != 5)
		{
			System.out.println("Enter 1 for Addition, 2 for Substraction, 3 for Multiplication, 4 for Division, 5 for exit");
			operation = sc.nextInt();
			operationComputation(operation, firstNum, secondNum);
	    }
	}
		
	public static void countNumber(int start,int end)
	{
		int count = 1;
		for(int number = start;number < end;number ++)
		{
			if(number % 5 == 0)
			{
				count ++;
			}
		}
		System.out.println(count);
	}
	public static void evenNumbers(int startIndex, int endIndex)
	{
		for(int number=startIndex; number<endIndex; number++)
		{
			if(number%2==0)
			{
				System.out.println(number);
			}
		}
	}
	public static void oddNumbers(int start1, int end1)
	{
		for(int number = start1;number < end1;number ++)
		{
			if(number % 2 !=0)
			{
				System.out.println(number);
			}	
		}
	}
	public static void divisible(int start2, int end2)
	{
		for(int number = start2;number < end2;number ++)
		{
			if(number % 3==0)
			{
				System.out.println(number);
			}
		}
	}
	public static void operationComputation(int operation, int firstNum, int secondNum)
	{
		if(operation == 1)
		{
			System.out.println("The Sum of two numbers is ");
			System.out.println(firstNum + secondNum);
		}
		else if(operation == 2)
		{
			System.out.println("The Sub of two numbers is ");
			System.out.println(firstNum - secondNum);		
			}
		
		else if(operation == 3)
		{
			System.out.println("The Mul of two numbers is ");
			System.out.println(firstNum * secondNum);	
		}
		else if(operation == 4)
		{	
			System.out.println("The Div of two numbers is ");
			System.out.println(firstNum / secondNum);	
		}

	}
}
