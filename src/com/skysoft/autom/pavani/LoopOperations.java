package com.skysoft.autom.pavani;
import java.util.Scanner;
public class LoopOperations
{
		public static void main(String[] args) 
		{
			int number;
			int firstNum;
			int secondNum;
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Enter first number");
			firstNum = sc.nextInt();
			
			System.out.println("Enter Second number");
			secondNum = sc.nextInt();
			
			int operation = 0;

			while(operation != 5)
			{
				System.out.println("Enter 1 for Addition, 2 for Substraction, 3 for Multiplication, 4 for Division, 5 for exit");
				operation = sc.nextInt();
				operationComputation(operation, firstNum, secondNum);
		    }
		}

		public static void operationComputation(int operation, int firstNum, int secondNum){
			if(operation == 1)
			{
				System.out.println("The Sum of two numbers is ");
				System.out.println(firstNum + secondNum);
			}
			else if(operation == 2)
			{
				System.out.println("The Sub of two numbers is ");
				System.out.println(firstNum - secondNum);		}
				
			else if(operation == 3)
			{
				System.out.println("The Mul of two numbers is ");
				System.out.println(firstNum * secondNum);	
			}
			else if(operation == 4)
			{
				System.out.println("The Div of two numbers is ");
				System.out.println(firstNum / secondNum);	
			}

		}
	}
