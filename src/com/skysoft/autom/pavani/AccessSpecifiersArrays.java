package com.skysoft.autom.pavani;

import java.util.ArrayList;
import java.util.Arrays;

public class AccessSpecifiersArrays
{
	public static void main(String[] args)
	{
			 int a[ ] = { 2,6,1,4,9,5 };
			 firstHighest(a);
			
			 int b[] = { 2,6,1,4,9,5 };
			 secondLowest(b);
			
			 int c[ ] = { 7,14,21,4,3,6,9 };
			 divisible(c);
			
			 int[] numbers = { 1,2,3,3,2,1};
			 duplicate(numbers);
			 
			
		}
		
		public static void firstHighest(int a[])
		{
			int number = a.length;
			Arrays.sort(a);
			System.out.println(Arrays.toString(a));
			int result = a[number - 1];
			System.out.println("Largest number is =" + result);
			System.out.println('\n');
		}
		private static void secondLowest(int b[])
		{
			int number = b.length;
			Arrays.sort(b);
			System.out.println(Arrays.toString(b));
			int result = b[1];
			System.out.println("Secound smallest number is =" + result);
		    System.out.println('\n');

		}
		protected static void divisible(int c[])
		{
			int count = 0;
			for(int i = 0 ; i < c[i] ; i ++)
			{
				if(c[i] % 7 == 0)
				{
					count ++;
				}
			}
			System.out.println("Count divisible by 7 = " +count);
		    System.out.println('\n');

		}
		
		 static void duplicate(int numbers[])
		{
				Arrays.sort(numbers);
			    int previous = numbers[0] - 1;
			    int dupCount = 0;
			    for (int i = 0; i < numbers.length; ++i)
			    {
			        if (numbers[i] == previous) 
			        {
			            ++dupCount;
			        } else
			        {
			            previous = numbers[i];
			        }
			    }
			    System.out.println("There were " + dupCount + " duplicate elements in the array.");
			    System.out.println('\n');

		}
		

	}
