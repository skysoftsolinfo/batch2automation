package com.skysoft.autom.pavani;
import java.io.IOException;
import java.util.Scanner;

public class MyInputScanner {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);		
		String name = in.nextLine();
		System.out.println(name);
		int a = in.nextInt();
		System.out.println(a);
	}
}
