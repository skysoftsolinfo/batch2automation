package com.skysoft.autom.pavani;

import java.util.ArrayList;
import java.util.Arrays;

public class Arraycomputations
{
	public static void main(String[] args)
	{
			 int a[ ] = { 2,6,1,4,9,5 };
			 firstHighest(a);
			
			 int b[] = { 2,6,1,4,9,5 };
			 secondLowest(b);
			
			 int c[ ] = { 7,14,21,4,3,6,9 };
			 divisible(c);
			
			 int[] numbers = { 1,2,3,3,2,1};
			 duplicate(numbers);
			 
			 int d[]={1,2,1,3,3};
			 uniquemethod(d);
			 ArrayList<Integer> unique = new ArrayList<Integer>();
		}
		
		public static void firstHighest(int a[])
		{
			int number = a.length;
			Arrays.sort(a);
			System.out.println(Arrays.toString(a));
			int result = a[number - 1];
			System.out.println("Largest number is =" + result);
			System.out.println('\n');
		}
		public static void secondLowest(int b[])
		{
			int number = b.length;
			Arrays.sort(b);
			System.out.println(Arrays.toString(b));
			int result = b[1];
			System.out.println("Secound smallest number is =" + result);
		    System.out.println('\n');

		}
		public static void divisible(int c[])
		{
			int count = 0;
			for(int i = 0 ; i < c[i] ; i ++)
			{
				if(c[i] % 7 == 0)
				{
					count ++;
				}
			}
			System.out.println("Count divisible by 7 = " +count);
		    System.out.println('\n');

		}
		
		public static void duplicate(int numbers[])
		{
				Arrays.sort(numbers);
			    int previous = numbers[0] - 1;
			    int dupCount = 0;
			    for (int i = 0; i < numbers.length; ++i)
			    {
			        if (numbers[i] == previous) 
			        {
			            ++dupCount;
			        } else
			        {
			            previous = numbers[i];
			        }
			    }
			    System.out.println("There were " + dupCount + " duplicate elements in the array.");
			    System.out.println('\n');

		}
		public static void uniquemethod(int d[])
		{
				ArrayList<Integer> unique = new ArrayList<Integer>();
				for(int i = 0 ; i <d.length-1 ; i ++)
				{
					int count = 0;
					for(int j = 0; j <d.length; j++)
					{
						if((d[i] == d[j]))
							count++;
					}
					if (count == 1)
						unique.add(d[i]);
				}
				System.out.println(unique.size());
				System.out.println(unique);
		}

	}
