package com.skysoft.autom.pavani;
public class AccessSpecifiersChar
{
	public static void main(String[] args) 
	{
		String str = "Hello";
		evenPosition(str);
		String str1 = "Hello";
		oddPosition(str);
	}
	public static void evenPosition(String s)
	{
		int even = 0;
		char str[] = new char[4];
		for(int i = 0 ; i < s.length() ; i ++)
		{
		if(i % 2 == 0)
		{
			str[even] = s.charAt(i);
			even ++;
		}
	}
		System.out.println("Even position in a given string is ::");
		System.out.println(str);
		System.out.println('\n');
	}

	private static void oddPosition(String s)
	{
		int odd = 0;
		char strodd[] = new char[3];
		for(int i = 0 ; i <s.length() ; i ++)
		{
			if(i % 2 == 1)
			{
				strodd[odd] = s.charAt(i);
				odd++;
			}
		}
		System.out.println("Odd position in a given string is ::");
		System.out.println(strodd);
	}
}
